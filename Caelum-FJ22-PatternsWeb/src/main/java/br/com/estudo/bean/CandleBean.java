package br.com.estudo.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.ChartModel;

import br.com.estudo.grafico.GeradorModelGrafico;
import br.com.estudo.indicadores.IndicadorFactory;
import br.com.estudo.main.CandleFactory;
import br.com.estudo.modelo.Candle;
import br.com.estudo.modelo.Negociacao;
import br.com.estudo.modelo.SerieTemporal;
import br.com.estudo.webService.ClientWebService;

@ManagedBean(name="CandleBean")
@ViewScoped
public class CandleBean implements Serializable{
	
	private static final long serialVersionUID = 8243296002413263205L;
	
	private List<Negociacao> negociacoes;
	private ChartModel modeloGrafico;
	private String nomeMedia;
	private String nomeIndicador;
	private String tituloGrafico;
	


	public CandleBean(){
		this.negociacoes = new ClientWebService().getNegociacoes();
		geraGrafico();			
	}

	public void geraGrafico() {
		
		List<Candle> candles = new CandleFactory().biuldCandles(negociacoes);
		SerieTemporal serie = new SerieTemporal(candles);
		
		GeradorModelGrafico gerador = new GeradorModelGrafico(serie,2, serie.getLastIndex(),tituloGrafico);
		gerador.plotaIndicador(new IndicadorFactory(nomeMedia, nomeIndicador).defineindicador());		
		this.modeloGrafico = gerador.getModeloGrafico();
	}	

	public List<Negociacao> getNegociacoes() {
		return this.negociacoes;
	}
	
	public ChartModel getModeloGrafico() {
		return modeloGrafico;
	}
	
	public String getNomeMedia() {
		return nomeMedia;
	}

	public String getNomeIndicador() {
		return nomeIndicador;
	}

	public void setNomeMedia(String nomeMedia) {
		this.nomeMedia = nomeMedia;
	}

	public void setNomeIndicador(String nomeIndicador) {
		this.nomeIndicador = nomeIndicador;
	}
	
	public String getTituloGrafico() {
		return tituloGrafico;
	}
	
	public void setTituloGrafico(String tituloGrafico) {
		this.tituloGrafico = tituloGrafico;
	}
}
