package br.com.estudo.bean;

import javax.faces.bean.ManagedBean;


@ManagedBean(name="MainBean")
public class MainBean {
	
	private String message;
	
	private String name;
	
	
	public void sendName(){
		String aux = name;
		
		if(aux.equalsIgnoreCase("Who are you?"))
			message = "You know who i'm... say my name!";
		if(aux.equalsIgnoreCase("Heisenberg"))
			message = "You are goddamit right!";
		System.out.print(aux);
	}
	
	public String getMessage() {
		return message;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
