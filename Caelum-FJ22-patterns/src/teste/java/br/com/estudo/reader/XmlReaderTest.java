package br.com.estudo.reader;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import br.com.estudo.modelo.Negociacao;

public class XmlReaderTest {

	@Test
	public void test() {
		String xmlDeTeste = "<list>" +
				"<negociacao>"+
				"<preco>43.5</preco>" +
				"<quantidade>1000</quantidade>" +
				"<data>" +
				"<time>1322233344455</time>" +
				"</data>" +
				"</negociacao>" +
				"</list>";
		XmlReader leitor = new XmlReader();
		InputStream xml = new ByteArrayInputStream(xmlDeTeste.getBytes());
		List<Negociacao> negociacoes = leitor.read(xml);
		
		assertEquals(1, negociacoes.size());
		assertEquals(43.5, negociacoes.get(0).getPreco(), 0.00001);
		assertEquals(1000, negociacoes.get(0).getQuantidade());
	}
	
	public void compareMiliseconds(){
		Calendar agora = Calendar.getInstance();
		Calendar mesmoMomento = (Calendar) agora.clone();
		Negociacao negociacao = new Negociacao(40.0, 100, agora);
		assertTrue(negociacao.compareDay(mesmoMomento));		
	}
}
