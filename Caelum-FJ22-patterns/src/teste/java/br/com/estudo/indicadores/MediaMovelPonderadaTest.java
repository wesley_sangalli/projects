package br.com.estudo.indicadores;

import static org.junit.Assert.*;
import org.junit.Test;
import br.com.estudo.modelo.SerieTemporal;
import br.com.estudo.utils.GeradorDeSerie;

public class MediaMovelPonderadaTest {

	@Test
	public void sequenciaSimplesDeCandles(){
		SerieTemporal serie = GeradorDeSerie.criaSerie(1, 2, 3, 4, 5, 6);
		Indicador mm = new MediaMovelPonderada(new IndicadorFechamento());
		
		assertEquals(14.0/6, mm.calcula(2, serie), 0.00001);
		assertEquals(20.0/6, mm.calcula(3, serie), 0.00001);
		assertEquals(26.0/6, mm.calcula(4, serie), 0.00001);
		assertEquals(32.0/6, mm.calcula(5, serie), 0.00001);
	}

}
