package br.com.estudo.main;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.estudo.modelo.Candle;
import br.com.estudo.modelo.Negociacao;

public class CandleFactory {
	public Candle candleForDate(Calendar data, List<Negociacao> negociacoes){
		
		double maximo = 0;
		double minimo = Double.MAX_VALUE;
		double volume = 0;
		
		for (Negociacao negociacao : negociacoes) {
			volume += negociacao.getVolume();
			double preco = negociacao.getPreco();
			if(preco>maximo){
				maximo = preco;
			}
			if(preco<minimo){
				minimo = preco;
			}
		}
		
		double abertura = negociacoes.isEmpty() ? 0 : negociacoes.get(0).getPreco();
		double fechamento = negociacoes.isEmpty() ? 0 : negociacoes.get(negociacoes.size()-1).getPreco();
				
		return new Candle(abertura, fechamento, minimo, maximo, volume, data);		
	}

	public List<Candle> biuldCandles(List<Negociacao> negociacoes) {
		List<Candle> candles = new ArrayList<Candle>();
		List<Negociacao> negociacaoByDay = new ArrayList<Negociacao>();
		
		Calendar atual = negociacoes.get(0).getData();
		
		for (Negociacao negociacao : negociacoes) {
			if (!negociacao.compareDay(atual)) {				
				Candle candleDoDia = candleForDate(atual,negociacaoByDay);
				candles.add(candleDoDia);
				negociacaoByDay = new ArrayList<Negociacao>();
				atual = negociacao.getData();
			}
			negociacaoByDay.add(negociacao);
		}
		Candle candleDoDia = candleForDate(atual,negociacaoByDay);
		candles.add(candleDoDia);
		return candles;
	}
}
