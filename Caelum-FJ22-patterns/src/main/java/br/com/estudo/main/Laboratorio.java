package br.com.estudo.main;

public class Laboratorio {

	public static void multipleOfFive(String hour) {

		String[] currHour = hour.split(":");
		int minutes = Integer.parseInt(currHour[1]);

		if (minutes == 00 || (minutes % 5 == 0)) {
			System.out.print(minutes);
		} else if (minutes > 0 && (minutes % 10) < 5) {
			while (minutes % 5 != 0) {
				minutes--;
			}
		} else if ((minutes % 10) > 5 && !(minutes % 5 == 0)) {
			while (minutes % 5 != 0) {
				minutes++;
			}
		}
		System.out.print(minutes);
	}

	public static void main(String[] args) {
		multipleOfFive(new String("12:37"));
	}
}