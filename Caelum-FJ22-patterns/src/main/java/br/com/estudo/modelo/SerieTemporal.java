package br.com.estudo.modelo;

import java.util.List;

public class SerieTemporal {
	private final List<Candle> candles;
		
	public SerieTemporal(List<Candle> candles){
		this.candles = candles;
	}
	
	public Candle getCandle(int index) {
		return this.candles.get(index);
	}
	
	public int getLastIndex(){
		return candles.size()-1;
	}

}
