package br.com.estudo.webService;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import br.com.estudo.modelo.Negociacao;
import br.com.estudo.reader.XmlReader;

public class ClientWebService {
	private static final String URL_WEBSERVICE = "http://argentumws.caelum.com.br/negociacoes";
	public ClientWebService(){
		
	}
	
	public List<Negociacao> getNegociacoes(){	
		HttpURLConnection connection = null;
		try {
			URL url = new URL(URL_WEBSERVICE);			
			connection = (HttpURLConnection)url.openConnection();
			InputStream content = connection.getInputStream();
			return new XmlReader().read(content);	
		} catch (IOException e) {			
			e.printStackTrace();
		}finally{
			connection.disconnect();
		}
		return null;
	}
}
