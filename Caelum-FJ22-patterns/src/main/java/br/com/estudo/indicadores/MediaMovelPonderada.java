package br.com.estudo.indicadores;

import br.com.estudo.modelo.Candle;
import br.com.estudo.modelo.SerieTemporal;

public class MediaMovelPonderada implements Indicador{
	
	
	private Indicador outroIndicador;

	public MediaMovelPonderada(Indicador outroIndicador) {
		this.outroIndicador = outroIndicador;	
	}

	public double calcula(int index, SerieTemporal serie){
		double soma = 0.0;
		int peso = 3;
		for(int i = index; i > index-3; i--){
			Candle c = serie.getCandle(i);
			soma+= c.getFechamento() * peso--;			
		}
		return soma /6;
	}

	@Override
	public String toString() {
		return "MMP de Fechamento";
	}
}
