package br.com.estudo.indicadores;

import java.lang.reflect.Constructor;

public class IndicadorFactory {
	private final String nomeMedia;
	private final String nomeIndicador;
	
	public IndicadorFactory(String nomeMedia, String nomeIndicador){
		this.nomeMedia = nomeMedia;
		this.nomeIndicador = nomeIndicador;
		
	}
	
	public Indicador defineindicador() {	
		if (nomeIndicador == null || nomeMedia == null)
			return new MediaMovelSimples(new IndicadorFechamento());
		try {
			String pacote = "br.com.estudo.indicadores.";
			Class<?> classeIndicador = Class.forName(pacote + nomeIndicador);
			Indicador indicadorBase = (Indicador) classeIndicador.newInstance();
			
			Class<?> classeMedia = Class.forName(pacote + nomeMedia);
			Constructor<?> construtorMedia = classeMedia.getConstructor(Indicador.class); 
			Indicador indicador = (Indicador) construtorMedia.newInstance(indicadorBase);
			return indicador;
		}catch (Exception e){		
			throw new RuntimeException(e);
		}
	}

}
