package br.com.estudo.indicadores;

import br.com.estudo.modelo.SerieTemporal;

public class MediaMovelSimples implements Indicador{
	

	private Indicador outroIndicador;

	public MediaMovelSimples(Indicador outroIndicador) {
		
		this.outroIndicador = outroIndicador;
	
	}
	
	public double calcula(int index, SerieTemporal serie){		
		double soma = 0.0;		
		for(int i = index; i > index-3; i--){
			soma+= outroIndicador.calcula(i, serie);		
		}
		
		return soma /3;
	}
	
	@Override
	public String toString() {
		return "MMS de Fechamento";
	}
}
