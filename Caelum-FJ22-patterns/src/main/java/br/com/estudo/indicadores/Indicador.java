package br.com.estudo.indicadores;

import br.com.estudo.modelo.SerieTemporal;

public interface Indicador {
	public double calcula(int index, SerieTemporal serie);

}
