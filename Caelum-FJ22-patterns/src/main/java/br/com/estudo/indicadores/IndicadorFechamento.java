package br.com.estudo.indicadores;

import br.com.estudo.modelo.SerieTemporal;

public class IndicadorFechamento implements Indicador {
	
	@Override
	public double calcula(int index, SerieTemporal serie) {
		return serie.getCandle(index).getFechamento();
	}
}
