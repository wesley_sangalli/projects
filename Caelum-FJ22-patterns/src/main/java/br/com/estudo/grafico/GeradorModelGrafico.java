package br.com.estudo.grafico;

import org.primefaces.model.chart.ChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import br.com.estudo.indicadores.Indicador;
import br.com.estudo.indicadores.MediaMovelSimples;
import br.com.estudo.modelo.SerieTemporal;

public class GeradorModelGrafico {
	private int comeco;
	private int fim;
	private SerieTemporal serie;
	private LineChartModel modeloGrafico;
	private String tituloGrafico;
	
	public GeradorModelGrafico(SerieTemporal serie, int comeco, int fim, String titulo){
		this.serie = serie;
		this.comeco = comeco;
		this.fim = fim;
		this.tituloGrafico = titulo;
		
		this.modeloGrafico = new LineChartModel();		
	}
	
	public void plotaIndicador(Indicador indicador){		
		LineChartSeries chartSeries = new LineChartSeries(indicador.toString());
		for(int i = comeco; i<= fim; i++){
			double value = indicador.calcula(i, serie);
			chartSeries.set(i, value);
		}
		this.modeloGrafico.addSeries(chartSeries);
		this.modeloGrafico.setLegendPosition("w");
		this.modeloGrafico.setTitle(tituloGrafico);
	}

	
	public ChartModel getModeloGrafico() {
		return modeloGrafico;
	}
	
	@Override
	public String toString() {
		return "GeradorModelGrafico [comeco=" + comeco + ", fim=" + fim
				+ ", serie=" + serie + ", modeloGrafico=" + modeloGrafico + "]";
	}	
}
