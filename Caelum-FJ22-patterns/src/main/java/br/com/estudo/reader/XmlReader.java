package br.com.estudo.reader;

import java.io.InputStream;
import java.util.List;

import br.com.estudo.modelo.Negociacao;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XmlReader {
	@SuppressWarnings("unchecked")
	public List<Negociacao> read(InputStream inputStream){
		XStream stream = new XStream(new DomDriver());
		stream.alias("negociacao", Negociacao.class);
		return (List<Negociacao>) stream.fromXML(inputStream);		
	}
}
